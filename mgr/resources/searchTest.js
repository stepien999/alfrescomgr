var ITERATIONS = 100;
var startDateFrom = "";
var startDateTo = "";
var endDateFrom = "";
var endDateTo = "";
var place = "";
var number = "150/2017";

var actualTasks = function(del) {
	var status = "";
	var parents = del.getParentAssocs();
	if (parents["bpm:packageContains"] == null
			|| parents["bpm:packageContains"].length == 0) {
		return "";
	}
	var workflowId = parents["bpm:packageContains"][0].properties["bpm:workflowInstanceId"];
	var workflowInstance = workflow.getInstance(workflowId);

	if (workflowInstance.isActive()) {
		var paths = workflowInstance.getPaths();
		var tasks = paths[0].getTasks();
		var tasksTab = [];
		for ( var i in tasks) {
			var taskProp = tasks[i].getProperties();
			var taskId = tasks[i].getId();

			var owner = taskProp["cm:owner"];
			var group = taskProp["bpm:pooledActors"];

			var name = "";
			if (owner != null) {
				name = owner;
			} else if (group != null) {
				var names = [];
				for (var j = 0; j < group.size(); ++j) {
					var candidateName = "";
					var nodeRef = "" + group.get(j);
					var node = search.findNode(nodeRef);
					logger.log(node.getType());
					if (node.getType() == "{http://www.alfresco.org/model/content/1.0}authorityContainer") {
						candidateName = node.properties["cm:authorityDisplayName"];
					} else {
						candidateName = node.properties["cm:userName"];
					}
					names.push(candidateName);
				}
				name = names.join(", ");
			}
			tasksTab.push(tasks[i].getTitle() + " (" + name + ")");
		}
		status = tasksTab.join(", ");
	}
	return status;
};

if (startDateFrom == "") {
	startDateFrom = "MIN";
}
if (startDateTo == "") {
	startDateTo = "MAX";
}
if (endDateFrom == "") {
	endDateFrom = "MIN";
}
if (endDateTo == "") {
	endDateTo = "MAX";
}

var query = "@del\\:number:*" + number + "*";
if (place != "") query += " AND @del\\:place:*" + place + "*";
if (startDateFrom != "MIN" || startDateTo != "MAX") query += " AND @del\\:startDate:[" + startDateFrom + " TO " + startDateTo + "]";
if (endDateFrom != "MIN" || endDateTo != "MAX") query += " AND @del\\:endDate:[" + endDateFrom + " TO " + endDateTo + "]";
logger.log(query);

var timeFrom = new Date().getTime();
for (var i = 0; i < ITERATIONS; i++) {
   var luceneResults = search.luceneSearch(query);
	logger.log("Liczba wynikow: " + luceneResults.length);
	for (var luceneResult in luceneResults) {
		actualTasks(luceneResults[luceneResult]);
		logger.log("Pobrano aktualne zadanie");
	}
}
var timeTo = new Date().getTime();

logger.log("Czas wykonania: " + (timeTo - timeFrom) + " [ms]");
