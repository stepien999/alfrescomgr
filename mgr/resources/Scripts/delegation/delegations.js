function rewriteTaskOutcome(resultName) {
	var result = task.getVariableLocal("mgrmodel_" + resultName);
	logger.warn("Wynik zadania: " + result);
	logger.warn("Nazwa zmiennej: mgrmodel_" + resultName);
	execution.setVariable("mgrmodel_" + resultName, result);
}

function rewriteFields() {
	var list = [ "del_startDate", "del_endDate", "del_target",
			"del_targetDescription", "del_department", "del_place",
			"del_transferDone", "del_car", "del_tripDescription", 
			"del_breakfasts", "del_dinners", "del_suppers" ];
	var nodeDel = bpm_package.children[0];

	for (var i = 0; i < list.length; ++i) {
		var val = task.getVariableLocal(list[i]);
		logger.warn(list[i] + ": " + val);
		if (val != null && val != 0) {
			execution.setVariable(list[i], val);
			nodeDel.properties[list[i].replace("_", ":")] = val;
		}
	}
	
	nodeDel.save();
	
	var listFloat = [ "del_advanceAmount", "del_allowance", "del_overallCost" ];
	for (var i = 0; i < listFloat.length; ++i) {
		var val = task.getVariableLocal(listFloat[i]);
		logger.warn(listFloat[i] + ": " + val);
		if (val != null) {
			execution.setVariable(listFloat[i], val);
			nodeDel.properties[listFloat[i].replace("_", ":")] = parseFloat(val);
		}
	}
	
	nodeDel.save();

	var tripDescription = task.getVariableLocal("del_tripDescription");

	if (tripDescription != null && tripDescription != "") {
		
		var oldTrips = nodeDel.childAssocs["del:trips"];
		if(oldTrips != null) {
			for(var i = 0; i < oldTrips.length; i++) {
				var element = oldTrips[i];
				element.remove();
			}
		}
		
		var tabOb = eval("(" + tripDescription + ")");
		for (var i = 0; i < tabOb.length; ++i) {
			var el = tabOb[i];
			var propMap = {
				"del:tripFrom" : el.tripFrom,
				"del:tripDateFrom" : el.tripDateFrom,
				"del:tripTimeFrom" : el.tripTimeFrom,
				"del:tripTo" : el.tripTo,
				"del:tripDateTo" : el.tripDateTo,
				"del:tripTimeTo" : el.tripTimeTo,
				"del:conveyance" : el.conveyance
			};
			nodeDel.createNode(null, "del:trip", propMap, "del:trips");
		}
	}
	nodeDel.save();
}

function errorMsg(text) {
	throw new Packages.org.alfresco.scripts.ScriptException("Błąd: " + text);
}