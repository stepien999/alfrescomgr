<import resource="/One/Data Dictionary/Scripts/delegation/delegations.js">

var startDate = task.getVariableLocal("del_startDate");
var endDate = task.getVariableLocal("del_endDate");
if(startDate == null || startDate == "") {
	errorMsg("Wypełnij datę wyjazdu");
}
if(endDate == null || endDate == "") {
	errorMsg("Wypełnij datę przyjazdu");
}

rewriteFields();
rewriteTaskOutcome("fillDelegationResult");

execution.setVariable("delegationEndDate", task.getVariableLocal("del_endDate"));
execution.setVariable("acceptor", "test_user1");
