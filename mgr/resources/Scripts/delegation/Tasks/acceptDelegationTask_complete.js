<import resource="/One/Data Dictionary/Scripts/delegation/delegations.js">

rewriteTaskOutcome("acceptDelegationResult");

if(task.getVariableLocal("del_startDate") > task.getVariableLocal("del_endDate")){
	execution.setVariable("skipWaiting", true);
}
else {
	execution.setVariable("skipWaiting", false);
}
