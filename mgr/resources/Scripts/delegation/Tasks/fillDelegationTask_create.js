<import resource="/One/Data Dictionary/Scripts/delegation/delegations.js">

var newNumber = ""+sequence.getNext("delegationNumber") + "/" + new Date().getFullYear();
logger.warn("Nowa delegacja o numerze: " + newNumber);
task.setVariableLocal("del_number", newNumber);
execution.setVariable("del_number", newNumber);

var nodeDel = bpm_package.children[0];
nodeDel.properties["del:number"] = newNumber;
nodeDel.save();
