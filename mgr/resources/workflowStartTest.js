var ITERATIONS = 500;

var newProcess = function() {
	var folder = search.xpathSearch("/app:company_home/cm:Delegacje/cm:Nowe")[0];
	var data = new Date();
	var del = folder.createFile("Delegacja "+data.getTime());
	del.addAspect("del:delegation");

	var action = actions.create("start-workflow");
	var userName = "admin";
	action.parameters["delegate"] = userName;
	
	del.properties["del:delegate"] = userName;
	del.save();
	
	action.parameters["workflowName"] = "activiti$delegation";
	action.parameters["bpm:workflowDescription"] = "Proces delegacji";
	action.execute(del);
};

var timeFrom = new Date().getTime();
for (var i = 0; i < ITERATIONS; i++) {
   newProcess();
}
var timeTo = new Date().getTime();

logger.log("Czas wykonania: " + (timeTo - timeFrom) + " [ms]");
