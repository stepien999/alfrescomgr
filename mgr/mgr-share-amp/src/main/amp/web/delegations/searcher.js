var buttonSearchId = "search-button";

YAHOO.util.Event.onContentReady(buttonSearchId, function() {
	
	var buttonSearch = document.getElementById(buttonSearchId);
	var number = document.getElementById("page_x002e_data-form_x002e_delegation-searcher_x0023_default_prop_del_number");
	var place = document.getElementById("page_x002e_data-form_x002e_delegation-searcher_x0023_default_prop_del_place");
	var startDate = document.getElementById("page_x002e_data-form_x002e_delegation-searcher_x0023_default_prop_del_startDate");
	var endDate = document.getElementById("page_x002e_data-form_x002e_delegation-searcher_x0023_default_prop_del_endDate");
	
	buttonSearch.onclick = function() {
		var startDateParam = startDate.value || "|";
		var endDateParam = endDate.value || "|";
		
		Alfresco.util.Ajax.jsonGet({ 
			
			url: Alfresco.constants.PROXY_URI+"pl/poznan/put/mgr/delegationSearcher",
			dataObj: {
				"number": number.value,
				"place": place.value,
				"startDateFrom": startDateParam.split("|")[0].replace(/T[0-9:+]+/, ""),
				"startDateTo": startDateParam.split("|")[1].replace(/T[0-9:+]+/, ""),
				"endDateFrom": endDateParam.split("|")[0].replace(/T[0-9:+]+/, ""),
				"endDateTo": endDateParam.split("|")[1].replace(/T[0-9:+]+/, "")
			},
			successCallback: {
				fn: function(data) {
					
					if(data.json.success) {
						
						console.log(data.json.list);
						
						var sortDates = function(recA,recB,desc,field) {
							var a = recA.getData(field).split(".");
							var b = recB.getData(field).split(".");
							
							return ((a[2]+a[1]+a[0])-(b[2]+b[1]+b[0])) * (desc ? -1 : 1);
						};
						
						var columns = [
						  { key:"number", label: "Numer", sortable: true },
						  { key:"target", label: "Rodzaj", sortable: true },
						  { key:"delegate", label: "Delegowany", sortable: true },
						  { key:"place", label: "Miejsce", sortable: true },
						  { key:"targetDescription", label: "Cel", sortable: true },
						  { key:"startDate", label: "Data wyjazdu", sortable: true, sortOptions: { sortFunction: sortDates }},
						  { key:"endDate", label: "Data przyjazdu", sortable: true, sortOptions: { sortFunction: sortDates }},
						  { key:"cost", label: "Koszt razem", sortable: true },
						  { key:"status", label: "Status", sortable: true }
						  
						];
						
						var dataSource = new YAHOO.util.DataSource(data.json.list);
						dataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
						dataSource.responseSchema = {
							fields: [ "number", "target", "delegate", "place", "targetDescription", "startDate", "endDate", "cost", "status", "nodeRef" ]	
								
						};
						
						var table = new YAHOO.widget.DataTable("results", columns, dataSource, {
							"MSG_EMPTY": "Brak wyników",
							"MSG_SORTDESC": "Sortuj malejąco",
							"MSG_SORTASC": "Sortuj rosnąco"
							
						});
					}
				}
			}
		});
	};
});