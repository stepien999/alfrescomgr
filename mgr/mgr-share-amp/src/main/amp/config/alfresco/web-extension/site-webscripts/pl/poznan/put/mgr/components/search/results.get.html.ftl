<@markup id="css" >
	<@link href="${url.context}/res/delegations/searcher.css" />
</@>

<@markup id="js">
   <@script type="text/javascript" src="${url.context}/res/components/form/date-range.js"></@script>
   <@script type="text/javascript" src="${url.context}/res/delegations/searcher.js"></@script>
   
</@>

<@markup id="widgets">
   <@createWidgets group="search"/>
</@>

<@markup id="html">
   <@uniqueIdDiv>
      <div class="results">
         <input type="button" name="Wyszukaj" value="Wyszukaj" id="search-button" />
         <div class="title">${msg("header")}</div>
         <div id="results">
         </div>
      </div>
   </@>
</@>