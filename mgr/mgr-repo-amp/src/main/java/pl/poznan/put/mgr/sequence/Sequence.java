package pl.poznan.put.mgr.sequence;

import org.alfresco.repo.processor.BaseProcessorExtension;

public class Sequence extends BaseProcessorExtension {
	
	private SequenceService sequenceService;
	
	public void defineSequence(String sequenceName){
		this.getSequenceService().defineSequence(sequenceName);
	}
	
	public long getNext(String sequenceName){
		return this.getSequenceService().getNext(sequenceName);
	}

	public SequenceService getSequenceService() {
		return sequenceService;
	}

	public void setSequenceService(SequenceService sequenceService) {
		this.sequenceService = sequenceService;
	}
}
