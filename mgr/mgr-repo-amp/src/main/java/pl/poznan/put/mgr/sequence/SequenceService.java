package pl.poznan.put.mgr.sequence;

public interface SequenceService {
	
	public void defineSequence(String sequenceName);

	public long getNext(String sequenceName);

}
