package pl.poznan.put.mgr.sequence;

import org.alfresco.service.namespace.QName;

public interface SequenceModel {
	
	public final static String NAMESPACE_SEQUENCE = "http://www.mgr.put.poznan.pl/model/sequence/1.0";

	public final static QName TYPE_SEQUENCE = QName.createQName(NAMESPACE_SEQUENCE, "sequence");
	public final static QName PROP_SEQUENCE_NAME = QName.createQName(NAMESPACE_SEQUENCE, "sequenceName");
	public final static QName PROP_SEQUENCE_START = QName.createQName(NAMESPACE_SEQUENCE, "sequenceStart");
	public final static QName PROP_SEQUENCE_DELTA = QName.createQName(NAMESPACE_SEQUENCE, "sequenceDelta");
	public final static QName PROP_SEQUENCE_VALUE = QName.createQName(NAMESPACE_SEQUENCE, "sequenceValue");
	public final static QName TYPE_SEQUENCES_CONTAINER = QName.createQName(NAMESPACE_SEQUENCE, "sequencesContainer");
	public final static QName ASSOC_SEQUENCES = QName.createQName(NAMESPACE_SEQUENCE, "sequences");
}
