package pl.poznan.put.mgr.repo.workflow.activiti.listener;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.StringTokenizer;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.el.Expression;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.importer.ImporterBootstrap;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.workflow.WorkflowQNameConverter;
import org.alfresco.repo.workflow.activiti.ActivitiScriptNode;
import org.alfresco.repo.workflow.activiti.script.ActivitiScriptBase;
import org.alfresco.scripts.ScriptException;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.workflow.WorkflowException;
import org.alfresco.service.namespace.NamespaceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import pl.poznan.put.mgr.repo.workflow.activiti.MgrActivitiConstants;
import pl.poznan.put.mgr.service.cmr.workflow.ExtendedException;


public class TaskScriptsListener extends ActivitiScriptBase implements ExecutionListener, TaskListener {

	private static final long serialVersionUID = 6331148573728620955L;

	private static final Log logger = LogFactory.getLog(TaskScriptsListener.class);

	private static final String PROPERTY_COMPANY_HOME_CHILDNAME = "spaces.company_home.childname";
	private static final String PROPERTY_DICTIONARY_CHILDNAME = "spaces.dictionary.childname";
	private static final String PROPERTY_SCRIPTS_FOLDER_CHILDNAME = "spaces.scripts.childname";
	private static final String TASK_BINDING_NAME = "task";
	
	private static final String TASKS_DEFAULT_FOLDER = "Tasks";
	private static final String PATHS_DEFAULT_FOLDER = "Paths";
	private static final String FLOW_DEFAULT_NAME = "flow";
	
	private Expression scriptFolder;
	private Expression scriptName;

	/*
	 * Executed during task events
	 */
	public void notify(DelegateTask delegateTask) {
		String scriptFolderName = getStringValue(scriptFolder, delegateTask);
		scriptFolderName = scriptFolderName != null ? scriptFolderName : TASKS_DEFAULT_FOLDER;
		
		String scriptFileName = getStringValue(scriptName, delegateTask);
		String taskId = delegateTask.getTaskDefinitionKey();
		scriptFileName = scriptFileName != null ? scriptFileName : taskId;
		
		String processId = delegateTask.getProcessDefinitionId().split(":")[0];
		String eventName = delegateTask.getEventName();
		String script = getScriptFromFile(processId, scriptFolderName, scriptFileName, eventName);
		if (script != null) {
			
			boolean clearAuthenticationContext = checkFullyAuthenticatedUser(delegateTask);
			try {
				String scriptProcessorName = getStringValue(scriptProcessor, delegateTask);
				String runAsUser = getStringValue(runAs, delegateTask);

				Map<String, Object> scriptModel = getInputMap(delegateTask, runAsUser);
				getServiceRegistry().getScriptService().buildCoreModel(scriptModel);

				if (logger.isDebugEnabled()) {
					logger.debug("execute: ");
				}
				
				Object scriptResult = executeScript(script, scriptModel, scriptProcessorName, runAsUser);
				
		        if ((scriptResult instanceof String) && (scriptResult != null))
		        {
		        	String stringResult = (String)scriptResult;
					if (stringResult.startsWith("exception:")&&(stringResult.length()>10)) {
		                throw new WorkflowException(stringResult.substring(10));
		        	} else if (stringResult.startsWith("extended exception:")&&(stringResult.length()>10)) {

		        		if(stringResult.startsWith("extended exception:;")){
		        			stringResult = stringResult.substring(20);
		        		}else{
		        			stringResult = stringResult.substring(19);
		        		}
		        	      StringTokenizer t = new StringTokenizer(stringResult, ";");
		        	      
		        	      String msgId = t.nextToken();
		        	      String variableName= t.nextToken();    // exception
		        		
		        	      List<String> varValues = new ArrayList<String>();
		        	      while(t.hasMoreTokens()){
		        	    	  varValues.add(t.nextToken());
		        	      }
		        		
		    			throw new ExtendedException (msgId, variableName, varValues);
		        	}
		        }
        	} catch (ExtendedException e) {
        		logger.error("ExtendedException:"+e.getLocalizedMessage());
				throw e;
        	} catch (WorkflowException e) {
        		logger.error("WorkflowException:"+e.getLocalizedMessage());
				throw e;
        	} catch (Exception e) {
        		logger.error("Exception:"+e.getLocalizedMessage());
				throw new WorkflowException(e.getLocalizedMessage());
			} finally {
				if (clearAuthenticationContext) {
					AuthenticationUtil.clearCurrentSecurityContext();
				}
			}
		} else {
			logger.warn("Script file for task "+taskId+" is not avaliable (process: "+processId+")");
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("notify end");
		}
		
	}

	/*
	 * Executed during transitions
	 */
	public void notify(DelegateExecution execution) throws Exception {
		String scriptFolderName = getStringValue(scriptFolder, execution);
		scriptFolderName = scriptFolderName != null ? scriptFolderName : PATHS_DEFAULT_FOLDER;
		String processId = execution.getProcessDefinitionId().split(":")[0];
		
		String scriptFileName = getStringValue(scriptName, execution);
		scriptFileName = scriptFileName != null ? scriptFileName : FLOW_DEFAULT_NAME;
		
		String script = getScriptFromFile(processId, scriptFolderName, scriptFileName, null);
		if (script != null) {
			String scriptProcessorName = getStringValue(scriptProcessor, execution);
			String runAsUser = getStringValue(runAs, execution);

			if (logger.isDebugEnabled()) {
				logger.debug("script: " + script);
				logger.debug("runAsUser: " + runAsUser);
			}
			
			boolean clearAuthenticationContext = checkFullyAuthenticatedUser(execution);
			Map<String, Object> scriptModel = getInputMap(execution, runAsUser);
			getServiceRegistry().getScriptService().buildCoreModel(scriptModel);
			
			if (logger.isDebugEnabled()) {
				logger.debug("execute2: ");
			}
			
			try {
				Object scriptResult = executeScript(script, scriptModel, scriptProcessorName, runAsUser);				
		        if ((scriptResult instanceof String) && (scriptResult != null))
		        {
		        	String stringResult = (String)scriptResult;
					if (stringResult.startsWith("exception:")&&(stringResult.length()>10)) {
		                throw new WorkflowException(stringResult.substring(10));
		        	} else if (stringResult.startsWith("extended exception:")&&(stringResult.length()>10)) {

		        		if(stringResult.startsWith("extended exception:;")){
		        			stringResult = stringResult.substring(20);
		        		}else{
		        			stringResult = stringResult.substring(19);
		        		}
		        	      StringTokenizer t = new StringTokenizer(stringResult, ";");
		        	      
		        	      String msgId = t.nextToken();
		        	      String variableName= t.nextToken();    // exception
		        		
		        	      List<String> varValues = new ArrayList<String>();
		        	      while(t.hasMoreTokens()){
		        	    	  varValues.add(t.nextToken());
		        	      }
		        		
		    			throw new ExtendedException (msgId, variableName, varValues);
		        	}
		        }
			}catch (ScriptException e){
				throw new ScriptException(e.getLocalizedMessage());
			} 
			finally {
				if (clearAuthenticationContext) {
					AuthenticationUtil.clearCurrentSecurityContext();
				}
			}
		} else {
			logger.warn("Script file "+scriptName+" is not avaliable");
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("notify end");
		}
		
	}

	private String getScriptFromFile(String process, String folder, String fileId, String eventName) {
		ImporterBootstrap bootstrap = getBootstrap();
		Properties configuration = bootstrap.getConfiguration();
		ServiceRegistry services = getServiceRegistry();

		String scriptsChildName = configuration.getProperty(PROPERTY_SCRIPTS_FOLDER_CHILDNAME);
		String companyHomeChildName = configuration.getProperty(PROPERTY_COMPANY_HOME_CHILDNAME);
		String dictionaryChildName = configuration.getProperty(PROPERTY_DICTIONARY_CHILDNAME);
		StoreRef storeRef = bootstrap.getStoreRef();
		
		String scriptFile = "cm:"+process+"/cm:"+folder+"/cm:"+fileId;
		if(eventName != null)
		{
			scriptFile += "_"+eventName;
		}
		scriptFile += ".js";
		
		NodeRef nodeRef;

		if (storeRef == null) {
			return null;
		}
		NodeRef storeRootNodeRef = services.getNodeService().getRootNode(storeRef);

		StringBuilder sb = new StringBuilder(256);
		sb.append("/").append(companyHomeChildName).append("/").append(dictionaryChildName).append("/").append(scriptsChildName).append("/")
				.append(scriptFile.replaceAll("\\\\", "/"));
		String xpath = sb.toString();

		List<NodeRef> nodeRefs = services.getSearchService().selectNodes(storeRootNodeRef, xpath, null, services.getNamespaceService(), false);

		if (nodeRefs.isEmpty()) {
			return null;
		} else {
			nodeRef = nodeRefs.get(0);
		}

		ContentReader cr = services.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
		if (cr == null || cr.exists() == false) {
			return null;
		}

		// get the content of the file
		String content = cr.getContentString();

		// get the root element
		SAXReader reader = new SAXReader();
		try {
			Document document = reader.read(new StringReader(content));
			Element root = document.getRootElement();
			boolean isTextOnly = true;
			for (Object e : root.elements()) {
				if (((Element) e).getNodeType() == Node.ELEMENT_NODE) {
					isTextOnly = false;
					break;
				}
			}
			if (isTextOnly) {
				return root.getTextTrim();
			} else {
				return root.elementTextTrim("expression");
			}
		} catch (DocumentException e) {
			return content;
		}
	}

	private ImporterBootstrap getBootstrap() {
		ProcessEngineConfigurationImpl config = Context.getProcessEngineConfiguration();
		if (config != null) {
			ImporterBootstrap bootstrap = (ImporterBootstrap) config.getBeans().get(MgrActivitiConstants.SPACES_BOOTSTRAP);
			if (bootstrap == null) {
				throw new RuntimeException("Spaces Bootstrap not present in ProcessEngineConfiguration beans, expected ServiceRegistry with key"
						+ MgrActivitiConstants.SPACES_BOOTSTRAP);
			}
			return bootstrap;
		}
		throw new IllegalStateException("No ProcessEngineCOnfiguration found in active context");
	}

	/**
	 * Checks a valid Fully Authenticated User is set. If none is set then
	 * attempts to set the workflow owner
	 * 
	 * @param execution
	 *            the execution
	 * @return <code>true</code> if the Fully Authenticated User was changed,
	 *         otherwise <code>false</code>.
	 */
	private boolean checkFullyAuthenticatedUser(final DelegateExecution execution) {
		if (AuthenticationUtil.getFullyAuthenticatedUser() == null) {
			NamespaceService namespaceService = getServiceRegistry().getNamespaceService();
			WorkflowQNameConverter qNameConverter = new WorkflowQNameConverter(namespaceService);
			String ownerVariableName = qNameConverter.mapQNameToName(ContentModel.PROP_OWNER);

			String userName = (String) execution.getVariable(ownerVariableName);
			if (userName != null) {
				AuthenticationUtil.setFullyAuthenticatedUser(userName);
				return true;
			}
		}
		return false;
	}

	private Map<String, Object> getInputMap(DelegateExecution execution, String runAsUser) {
		HashMap<String, Object> scriptModel = new HashMap<String, Object>(1);

		// Add current logged-in user and it's user home
		ActivitiScriptNode personNode = getPersonNode(runAsUser);
		if (personNode != null) {
			ServiceRegistry registry = getServiceRegistry();
			scriptModel.put(PERSON_BINDING_NAME, personNode);
			NodeRef userHomeNode = (NodeRef) registry.getNodeService().getProperty(personNode.getNodeRef(), ContentModel.PROP_HOMEFOLDER);
			if (userHomeNode != null) {
				scriptModel.put(USERHOME_BINDING_NAME, new ActivitiScriptNode(userHomeNode, registry));
			}
		}

		// Add activiti-specific objects
		scriptModel.put(EXECUTION_BINDING_NAME, execution);

		// Add all workflow variables to model
		Map<String, Object> variables = execution.getVariables();

		for (Entry<String, Object> varEntry : variables.entrySet()) {
			scriptModel.put(varEntry.getKey(), varEntry.getValue());
		}

		return scriptModel;
	}

	/**
	 * Checks a valid Fully Authenticated User is set. If none is set then
	 * attempts to set the task assignee as the Fully Authenticated User.
	 * 
	 * @param delegateTask
	 *            the delegate task
	 * @return <code>true</code> if the Fully Authenticated User was changed,
	 *         otherwise <code>false</code>.
	 */
	private boolean checkFullyAuthenticatedUser(final DelegateTask delegateTask) {
		if (AuthenticationUtil.getFullyAuthenticatedUser() == null) {
			String userName = delegateTask.getAssignee();
			if (userName != null) {
				AuthenticationUtil.setFullyAuthenticatedUser(userName);
				return true;
			}
		}
		return false;
	}

	protected Map<String, Object> getInputMap(DelegateTask delegateTask, String runAsUser) {
		HashMap<String, Object> scriptModel = new HashMap<String, Object>(1);

		// Add current logged-in user and it's user home
		ActivitiScriptNode personNode = getPersonNode(runAsUser);
		if (personNode != null) {
			ServiceRegistry registry = getServiceRegistry();
			scriptModel.put(PERSON_BINDING_NAME, personNode);
			NodeRef userHomeNode = (NodeRef) registry.getNodeService().getProperty(personNode.getNodeRef(), ContentModel.PROP_HOMEFOLDER);
			if (userHomeNode != null) {
				scriptModel.put(USERHOME_BINDING_NAME, new ActivitiScriptNode(userHomeNode, registry));
			}
		}

		// Add activiti-specific objects
		scriptModel.put(TASK_BINDING_NAME, delegateTask);
		scriptModel.put(EXECUTION_BINDING_NAME, delegateTask.getExecution());

		// Add all workflow variables to model
		Map<String, Object> variables = delegateTask.getExecution().getVariables();

		for (Entry<String, Object> varEntry : variables.entrySet()) {
			scriptModel.put(varEntry.getKey(), varEntry.getValue());
		}
		return scriptModel;
	}


	public void setScriptFolder(Expression scriptFolder) {
		this.scriptFolder = scriptFolder;
	}
	
	public void setScriptName(Expression scriptName) {
		this.scriptName = scriptName;
	}


}
