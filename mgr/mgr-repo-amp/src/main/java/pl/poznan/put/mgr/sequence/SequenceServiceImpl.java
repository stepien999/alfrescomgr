package pl.poznan.put.mgr.sequence;

import java.util.List;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.namespace.NamespacePrefixResolver;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;

public class SequenceServiceImpl implements SequenceService {

	private ServiceRegistry serviceRegistry;
	private NamespacePrefixResolver namespacePrefixResolver;
	private StoreRef storeRef;

	private static final QName SYSTEM_FOLDER_QNAME = QName.createQName(NamespaceService.SYSTEM_MODEL_1_0_URI, "system");
	private static final QName SEQUENCES_FOLDER_QNAME = QName.createQName(SequenceModel.NAMESPACE_SEQUENCE,
			"sequences");

	@Override
	public void defineSequence(String sequenceName) {
		NodeService nodeService = this.getServiceRegistry().getNodeService();
		NodeRef sequencesNodeRef = this.retrieveSequencesRoot();
		List<ChildAssociationRef> children = nodeService.getChildAssocs(sequencesNodeRef, RegexQNamePattern.MATCH_ALL,
				QName.createQName(SequenceModel.NAMESPACE_SEQUENCE, sequenceName), false);
		NodeRef sequence = null;
		if (children.size() > 0) {
			sequence = children.get(0).getChildRef();
			Long sequenceStartOld = (Long) nodeService.getProperty(sequence, SequenceModel.PROP_SEQUENCE_START);
			Long sequenceDeltaOld = (Long) nodeService.getProperty(sequence, SequenceModel.PROP_SEQUENCE_DELTA);
			if ((sequenceStartOld == null) || (!sequenceStartOld.equals(1)) || (sequenceDeltaOld == null)
					|| (!sequenceDeltaOld.equals(1))) {
				throw new AlfrescoRuntimeException("Incompatible sequence \"" + sequenceName + "\" exists: start = "
						+ sequenceStartOld + ", delta = " + sequenceDeltaOld);
			}
		} else {
			sequence = nodeService.createNode(sequencesNodeRef, SequenceModel.ASSOC_SEQUENCES,
					QName.createQName(SequenceModel.NAMESPACE_SEQUENCE, sequenceName), SequenceModel.TYPE_SEQUENCE)
					.getChildRef();
			nodeService.setProperty(sequence, SequenceModel.PROP_SEQUENCE_START, 1);
			nodeService.setProperty(sequence, SequenceModel.PROP_SEQUENCE_DELTA, 1);
		}
	}

	@Override
	public long getNext(String sequenceName) {
		NodeService nodeService = this.getServiceRegistry().getNodeService();
		NodeRef sequencesNodeRef = this.retrieveSequencesRoot();
		List<ChildAssociationRef> children = nodeService.getChildAssocs(sequencesNodeRef, RegexQNamePattern.MATCH_ALL,
				QName.createQName(SequenceModel.NAMESPACE_SEQUENCE, sequenceName), false);
		NodeRef sequence = null;
		Long sequenceStart = null;
		Long sequenceDelta = null;
		if (children.size() > 0) {
			sequence = children.get(0).getChildRef();
			sequenceStart = (Long) nodeService.getProperty(sequence, SequenceModel.PROP_SEQUENCE_START);
			sequenceDelta = (Long) nodeService.getProperty(sequence, SequenceModel.PROP_SEQUENCE_DELTA);
			if ((sequenceStart == null) || (sequenceDelta == null)) {
				throw new AlfrescoRuntimeException("Invalid sequence \"" + sequenceName + "\": start = " + sequenceStart
						+ ", delta = " + sequenceDelta);
			}
		} else {
			throw new AlfrescoRuntimeException("Sequence \"" + sequenceName + "\" is not exist");
		}
		Long sequenceValue = (Long) nodeService.getProperty(sequence, SequenceModel.PROP_SEQUENCE_VALUE);
		if (sequenceValue != null) {
			sequenceValue += sequenceDelta;
		} else {
			sequenceValue = sequenceStart;
		}
		nodeService.setProperty(sequence, SequenceModel.PROP_SEQUENCE_VALUE, sequenceValue);
		return sequenceValue;
	}

	private NodeRef getSequencesRoot() {
		NodeService nodeService = this.getServiceRegistry().getNodeService();
		NodeRef rootNodeRef = nodeService.getRootNode(this.getStoreRef());
		List<ChildAssociationRef> children = nodeService.getChildAssocs(rootNodeRef, RegexQNamePattern.MATCH_ALL,
				SYSTEM_FOLDER_QNAME, false);
		if (children.size() == 0) {
			throw new AlfrescoRuntimeException("Required system path not found: "
					+ SYSTEM_FOLDER_QNAME.toPrefixString(this.getNamespacePrefixResolver()));
		}
		NodeRef systemNodeRef = children.get(0).getChildRef();
		children = nodeService.getChildAssocs(systemNodeRef, RegexQNamePattern.MATCH_ALL, SEQUENCES_FOLDER_QNAME,
				false);
		NodeRef sequencesNodeRef = null;
		if (children.size() > 0) {
			sequencesNodeRef = children.get(0).getChildRef();
		}
		return sequencesNodeRef;
	}

	private NodeRef createSequencesRoot() {
		NodeService nodeService = this.getServiceRegistry().getNodeService();
		NodeRef rootNodeRef = nodeService.getRootNode(this.getStoreRef());
		List<ChildAssociationRef> children = nodeService.getChildAssocs(rootNodeRef, RegexQNamePattern.MATCH_ALL,
				SYSTEM_FOLDER_QNAME, false);
		if (children.size() == 0) {
			throw new AlfrescoRuntimeException("Required system path not found: "
					+ SYSTEM_FOLDER_QNAME.toPrefixString(this.getNamespacePrefixResolver()));
		}
		NodeRef systemNodeRef = children.get(0).getChildRef();
		NodeRef sequencesNodeRef = null;
		if (systemNodeRef != null) {
			sequencesNodeRef = nodeService.createNode(systemNodeRef, ContentModel.ASSOC_CHILDREN,
					SEQUENCES_FOLDER_QNAME, SequenceModel.TYPE_SEQUENCES_CONTAINER).getChildRef();
		}
		return sequencesNodeRef;
	}

	private NodeRef retrieveSequencesRoot() {
		NodeRef sequencesRoot = this.getSequencesRoot();
		if (sequencesRoot == null) {
			sequencesRoot = this.createSequencesRoot();
		}
		return sequencesRoot;
	}

	public ServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}

	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}

	public NamespacePrefixResolver getNamespacePrefixResolver() {
		return namespacePrefixResolver;
	}

	public void setNamespacePrefixResolver(NamespacePrefixResolver namespacePrefixResolver) {
		this.namespacePrefixResolver = namespacePrefixResolver;
	}

	public StoreRef getStoreRef() {
		return storeRef;
	}

	public void setStoreRef(StoreRef storeRef) {
		this.storeRef = storeRef;
	}

}
