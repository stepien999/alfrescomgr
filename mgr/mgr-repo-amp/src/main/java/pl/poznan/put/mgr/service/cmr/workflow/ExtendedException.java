package pl.poznan.put.mgr.service.cmr.workflow;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.jbpm.JbpmException;

public class ExtendedException extends JbpmException{

	private static final long serialVersionUID = 1273755185930926039L;
	private List<String> warningValues;
    private String warningVariable;
	
	public ExtendedException(String msgId, String variable, List<String> values, Object ... args)
    {
        super(msgId);
        this.setWarningVariable(variable);
        this.setWarningValues(values);
    }

	public ExtendedException(String msgId, String variable, Set<String> values, Object ... args)
    {
        super(msgId);
        this.setWarningVariable(variable);
        List<String> list = new ArrayList<String>(values);
        this.setWarningValues(list);
    }
	
	public void setWarningValues(List<String> warningValues) {
		this.warningValues = warningValues;
	}

	public List<String> getWarningValues() {
		return warningValues;
	}

	public void setWarningVariable(String warningVariable) {
		this.warningVariable = warningVariable;
	}

	public String getWarningVariable() {
		return warningVariable;
	}

	
}
