var newProcess = function() {
	
	var folder = search.xpathSearch("/app:company_home/cm:Delegacje/cm:Nowe")[0];
	var data = new Date();
	var del = folder.createFile("Delegacja "+data.getTime());
	del.addAspect("del:delegation");

	var action = actions.create("start-workflow");
	var userName = null;
	if (args.testMode == "true") userName = "admin";
	else userName = person.properties["cm:userName"]
	action.parameters["delegate"] = userName;
	
	del.properties["del:delegate"] = userName;
	del.save();
	
	action.parameters["workflowName"] = "activiti$delegation";
	action.parameters["bpm:workflowDescription"] = "Proces delegacji";
	action.execute(del);

	var rodzice = del.getParentAssocs();
	var workflowId = rodzice["bpm:packageContains"][0].properties["bpm:workflowInstanceId"];
	var workflowInstance = workflow.getInstance(workflowId);
	var paths = workflowInstance.getPaths();
	var tasks = paths[0].getTasks();
	var taskId = tasks[0].getId();
	
	return {
		nodeRef: del.getNodeRef(),
		workflowId: workflowId,
		taskId: taskId
	};
	
}

var main = function() {
	
	var result = {};
	result.success = false;
	
	try {
		
		if(args.action == null || args.action == "") {
			result.errorMsg = "Brak akcji";
			return result;
		}
		if(args.action == "newProcess") {
			result.workflow = newProcess();
			
		}		
	}
	catch(e) {
		logger.warn("Error: "+e);
		result.errorMsg = ""+e;
		return result;
	}
	
	
	result.success = true;
	return result;
}

model.result =  jsonUtils.toJSONString(main());