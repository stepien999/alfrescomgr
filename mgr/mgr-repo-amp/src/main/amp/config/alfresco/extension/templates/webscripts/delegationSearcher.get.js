var actualTasks = function(del) {

	var status = "";
	var parents = del.getParentAssocs();
	if (parents["bpm:packageContains"] == null
			|| parents["bpm:packageContains"].length == 0) {
		return "";
	}
	var workflowId = parents["bpm:packageContains"][0].properties["bpm:workflowInstanceId"];
	var workflowInstance = workflow.getInstance(workflowId);

	if (workflowInstance.isActive()) {
		var paths = workflowInstance.getPaths();
		var tasks = paths[0].getTasks();

		var tasksTab = [];
		for ( var i in tasks) {
			var taskProp = tasks[i].getProperties();
			var taskId = tasks[i].getId();

			var owner = taskProp["cm:owner"];
			var group = taskProp["bpm:pooledActors"];

			var name = "";
			if (owner != null) {
				name = owner;
			} else if (group != null) {
				var names = [];
				for (var j = 0; j < group.size(); ++j) {
					var candidateName = "";
					var nodeRef = "" + group.get(j);
					var node = search.findNode(nodeRef);
					logger.log(node.getType());
					if (node.getType() == "{http://www.alfresco.org/model/content/1.0}authorityContainer") {
						candidateName = node.properties["cm:authorityDisplayName"];
					} else {
						candidateName = node.properties["cm:userName"];
					}
					names.push(candidateName);
				}
				name = names.join(", ");
			}
			tasksTab.push(tasks[i].getTitle() + " (" + name + ")");
		}
		status = tasksTab.join(", ");
	}
	return status;
}

var startDateFrom = args.startDateFrom || "";
var startDateTo = args.startDateTo || "";
var endDateFrom = args.endDateFrom || "";
var endDateTo = args.endDateTo || "";
var place = args.place || "";
var number = args.number || "";

if (startDateFrom == "") {
	startDateFrom = "MIN";
}
if (startDateTo == "") {
	startDateTo = "MAX";
}
if (endDateFrom == "") {
	endDateFrom = "MIN";
}
if (endDateTo == "") {
	endDateTo = "MAX";
}

var query = "@del\\:number:*" + number + "*";
if (place != "") query += " AND @del\\:place:*" + place + "*";
if (startDateFrom != "MIN" || startDateTo != "MAX") query += " AND @del\\:startDate:[" + startDateFrom + " TO " + startDateTo + "]";
if (endDateFrom != "MIN" || endDateTo != "MAX") query += " AND @del\\:endDate:[" + endDateFrom + " TO " + endDateTo + "]";

var luceneResults = search.luceneSearch(query);

var list = [];
var ob = {};
var formatter = new Packages.java.text.SimpleDateFormat("dd.MM.YYYY");
for (var i in luceneResults) {
	
	var startDate = luceneResults[i].properties["del:startDate"];
	if (startDate != null) {
		startDate = "" + formatter.format(startDate);
	}
	
	var endDate = luceneResults[i].properties["del:endDate"];
	if (endDate != null) {
		endDate = "" + formatter.format(endDate);
	}
	
	var actualTaskString = actualTasks(luceneResults[i]);
	if (actualTaskString == "") actualTaskString = "Koniec procesu";

	list.push({
		"number" : luceneResults[i].properties["del:number"],
		"place" : luceneResults[i].properties["del:place"],
		"startDate" : startDate,
		"endDate" : endDate,
		"status" : actualTaskString,
		"nodeRef" : luceneResults[i].getNodeRef(),
		"delegate" : luceneResults[i].properties["del:delegate"],
		"target" : luceneResults[i].properties["del:target"],
		"targetDescription" : luceneResults[i].properties["del:targetDescription"],
		"cost" : luceneResults[i].properties["del:overallCost"]
	});
}
ob.list = list;

logger.warn("Delegation search result: " + list.length);

ob.success = true;

model.result = jsonUtils.toJSONString(ob);